

import React, { useState, useEffect } from 'react';
import { Calculator } from './Calculator';

export const App = () => {
  const [details, setDetails] = useState([]);
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');

  useEffect(() => {
    getDataLs();
  }, []);

  const getDataLs = () => {
    let storedDetails = JSON.parse(localStorage.getItem('details'));
    if (storedDetails) {
      setDetails(storedDetails);
    }
  }

  const handelSubmit = () => {
    let detail = { name, address };
    let updatedDetails = [...details, detail];
    setDetails(updatedDetails);
    localStorage.setItem('details', JSON.stringify(updatedDetails));
    setName('');
    setAddress('');
  }


  const handelEdit = (index) => {
    setName(details[index].name);
    setAddress(details[index].address);
    let updatedList = [...details];
    updatedList.splice(index, 1);
    setDetails(updatedList);
    localStorage.setItem('details', JSON.stringify(updatedList));
  }

  const handelDelete = (index) => {
    let updatedList = [...details];
    updatedList.splice(index, 1);
    setDetails(updatedList);
    localStorage.setItem('details', JSON.stringify(updatedList));
  }

  return (
    <div className='row'>
      <div className='col-4'>
        <dt>NAME</dt>
        <dd><input type='text' value={name} onChange={(e) => setName(e.target.value)} placeholder='Enter Your Name' required /></dd>
        <dt>ADDRESS</dt>
        <dd><input type='text' value={address} onChange={(e) => setAddress(e.target.value)} placeholder='Enter Your Address' required /></dd>
        <button type='submit' onClick={handelSubmit} className='btn btn-success'>Submit</button>
      </div>
      <div className='col-4'>
        <table className='table table-responsive'>
          <thead>
            <tr>
              <th>NAME</th>
              <th>ADDRESS</th>
              <th>EDIT</th>
              <th>DELETE</th>
            </tr>
          </thead>
          <tbody>
            {details.map((item, index) => (
              <tr key={index}>
                <td>{item.name}</td>
                <td>{item.address}</td>
                <td><button onClick={() => handelEdit(index)} className='btn btn-warning'>edit</button></td>
                <td><button onClick={() => handelDelete(index)} className='btn btn-danger'>delete</button></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className='col-4'>
        <Calculator />
      </div>
    </div>
  )
}

export default App;
