import React,{ useState} from "react"
import './Calculator.css';
export const Calculator = () => {
    const [result,setResult]=useState("");

    const calClick=(e)=>{
        setResult(result.concat(e.target.value));
        // console.log(result);
    }
    const clearClick=()=>{
      setResult("");
    }
    const resClick=()=>{
      setResult(eval(result));
    }
    
    return (
      <div className="Calculator">
        <input type='text' placeholder="0" id='calculate' value={result} />
        <input type='button' value='1' className="button" onClick={calClick}/>
        <input type='button' value='2' className="button" onClick={calClick} />
        <input type='button' value='3' className="button" onClick={calClick}/>
        <input type='button' value='4' className="button" onClick={calClick}/>
        <input type='button' value='5' className="button" onClick={calClick}/>
        <input type='button' value='6' className="button" onClick={calClick} />
        <input type='button' value='7' className="button" onClick={calClick} />
        <input type='button' value='8' className="button" onClick={calClick} />
        <input type='button' value='9' className="button" onClick={calClick}/>
        <input type='button' value='0' className="button" onClick={calClick} />
        <input type='button' value='+' className="button" onClick={calClick} />
        <input type='button' value='-' className="button" onClick={calClick} />
        <input type='button' value='*' className="button" onClick={calClick} />
        <input type='button' value='/' className="button" onClick={calClick} />
        <input type='button' value='Clear' className="button btn1" onClick={clearClick} />
        <input type='button' value='=' className="button btn1" onClick={resClick} />

      </div>
    )
  }


