import React,{useState , useEffect} from 'react'
// import { Table } from './Table';
import { Calculator } from './Calculator';

export const App = () => {

  const [details,setDetails] = useState([]);
  const [name,setName] = useState('');
  const [address,setAddress] = useState('');
  // const [edit,setEdit] = useState();

  const handelSubmit = () =>{
    let detail = {name,address};
    setDetails([...details,detail]);
    localStorage.setItem('details',JSON.stringify(details));
    setName('');
    setAddress('');
  }
useEffect(()=>{
  getDataLs();
},[])

  const getDataLs = () =>{
  let b=JSON.parse(localStorage.getItem('details'))
  if(b){
    setDetails(b);
  }
  }
  const handelEdit = (index) =>{

    setName(details[index].name);
    setAddress(details[index].address);
    
  }
  const handelDelete = (index) =>{
   
    let updatedList = [...details];
    updatedList.splice(index,1);
    setDetails(updatedList);
    localStorage.setItem('details',JSON.stringify(updatedList));
    
  }
  // console.log("data",data)
  return (
    <div className='row' >
      <div className='col-4'>
        <dt>NAME</dt>
        <dd><input type='text' value={name} onChange={(e)=>setName(e.target.value)} placeholder='Enter Your Name' required /></dd>
        <dt>ADDRESS</dt>
        <dd><input type='text' value={address} onChange={(e)=>setAddress(e.target.value)} placeholder='Enter Your Address' required /></dd>
        <button type='submit' onClick={handelSubmit} className='btn btn-success'>Submit</button>
      </div>
      <div className='col-4'>
      <table className='table table-responsive'>
          <thead>
            <th>NAME</th>
            <th>ADDRESS</th>
            <th>EDIT</th>
            <th>DELETE</th>
          </thead>
          <tbody>
            {details.map((item, index) => (
              <tr key={index}>
                <td>{item.name}</td>
                <td>{item.address}</td>
                <td><button onClick={()=>handelEdit(index)} className='btn btn-warning'>edit</button></td>
                <td><button onClick={()=>handelDelete(index)} className='btn btn-danger'>delete</button></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className='col-4'>
        <Calculator />
      </div>
    </div>
  )
}

export default App
